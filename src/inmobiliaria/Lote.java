package inmobiliaria;

import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Paula
 */
public class Lote implements Serializable {
    private int numLote;
    private int numMz;
    private double sizeLote; //en metros cuadrados
    
    public Lote(int numLote, int numMz, double sizeLote){
        this.numLote = numLote;
        this.numMz = numMz;
        this.sizeLote = sizeLote;
    }
    
    public int getnumLote(){
        return numLote;
    }
    
    public int getnumMz(){
        return numMz;
    }
    
    public double getsizeLote(){
        return sizeLote;
    }
    
    
}
