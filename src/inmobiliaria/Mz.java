package inmobiliaria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Paula
 */
public class Mz implements Serializable{
    private int numLotes;
    private int numMz;
    private ArrayList<Lote> lotes;
    
    static Scanner sc = new Scanner(System.in);
    /**
     * Constructor nuevo de Manzanas
     * @param numMz número de la manzana
     */
    public Mz(int numMz){
        this.numMz = numMz;
    }
    
    /**
     * Constructor de Mz cuando ya hay lotes añadidos
     * @param numLotes número de lotes 
     * @param numMz número de manzana
     * @param lotes Lotes en Manzana
     */
    public Mz(int numLotes, int numMz, ArrayList<Lote> lotes){
        this.numLotes = numLotes;
        this.numMz = numMz;
        this. lotes = lotes;
    }
    
    /**
     * Constructor de Mz cuando no hay lotes añadidos
     * @param numLotes número de lotes
     * @param numMz número de manzana
     */
    public Mz(int numLotes, int numMz){
        this.numLotes = numLotes;
        this.numMz = numMz;
    }
    
    public int getnumLotes(){
        return numLotes;
    }
    
    public int getnumMz(){
        return numMz;
    }
        
    public ArrayList<Lote> getLotes(){
        return lotes;
    }
    
    //Metodos
    
    /**
     * Añade un Lote a la Manzana
     */
    public void addLote(){//revisar *****
        System.out.println("Ingrese el número de la manzana: ");
        int numMz = sc.nextInt();
        System.out.println("Ingrese número de lote: ");
        int numLote = sc.nextInt();
        System.out.println("Ingrese el tamaño del lote en metros cuadrados: ");
        double sizeLote = sc.nextDouble();
        
        Lote l = new Lote(numLote, numMz, sizeLote);
        lotes.add(l);
    }
    
    /**
     * Elimina el lote ingresado
     */
    public void delLote(){
        System.out.println("Ingrese el número de lote a eliminar"); /////
        int lot = sc.nextInt();
        for (Lote lote:lotes){
            if(lote.getnumLote() == lot){
               lotes.remove(lote); 
            }
            else{
                System.out.println("Lote ingresado no existe. ");
            }
        }
        
    }
}
