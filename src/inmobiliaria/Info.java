/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inmobiliaria;

import java.io.Serializable;
import java.io.*;
import java.util.*;
/**
 *
 * @author Andres
 */
public class Info implements Serializable {
     public static void writeInfo(ArrayList<Urbanization> urb){
        try{
            ObjectOutputStream write= new ObjectOutputStream(new FileOutputStream("urb.txt"));
            write.writeObject(urb);
            write.close();
        }
        catch(Exception e){
            System.out.println("ERROR!!!");
        }
    }
      public static  ArrayList<Urbanization> readInfo(){
        
        try{
            ObjectInputStream read= new ObjectInputStream(new FileInputStream("urb.txt"));
            ArrayList<Urbanization> urb=(ArrayList<Urbanization>)read.readObject();
            read.close();
            return urb;
        }
        catch(Exception e){
            System.out.println("¡ERROR, no se encontro el archivo!");
        }
        return null;
    }
    
}
