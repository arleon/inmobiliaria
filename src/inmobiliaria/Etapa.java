package inmobiliaria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Angellina
 */
public class Etapa implements Serializable{
    private String nombre;
    private boolean onSale;
    private int numMz;
    private ArrayList<Mz> manzanas;
    
    static Scanner sc = new Scanner(System.in);
    /**
     * Constructor de Etapa
     * @param nombre Nombre de la etapa
     * @param onSale Si se encuentra la etapa en venta o no
     * @param numMz Número de manzanas en la etapa
     * @param manzanas Manzanas en etapa
     */
    public Etapa(String nombre, boolean onSale, int numMz, ArrayList<Mz> manzanas){
        this.nombre = nombre;
        this.onSale = onSale;
        this.numMz = numMz;
        this.manzanas = manzanas; 
    }
    public Etapa(String nombre, int numMz){
        this.nombre = nombre;
        this.onSale = true;
        this.numMz = numMz;
    }
    
    //getters        this.manzanas = manzanas; 

    public String getNombre(){
        return nombre;
    }
    
    public boolean getonSale(){
        return onSale;
    }
    
    public int getnumMz(){
        return numMz;
    }
    
    public ArrayList<Mz> getManzanas(){
        return manzanas;
    }
    
    public String toString(){
        String a;
        if(onSale == true){
            a = "Nombre de la etapa: "+nombre+"\nEstado: en venta\nNúmero de manzanas existentes: "+numMz;
        }
        else{
            a = "Nombre de la etapa: "+nombre+"\nEstado: vendida\nNúmero de manzanas vendidas: "+numMz;
        }
        return a;
        //override here
    }

    
 
    
    
    
}
