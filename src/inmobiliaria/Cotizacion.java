/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inmobiliaria;

/**
 *
 * @author Andres
 */
public class Cotizacion {
    //Aqui se crean las variables para la cotizacion
    private boolean house, dep, isMarried;
    private int numDorms, numBaths, numFloors;
    private String name, email, sector;
    private double income, $entrada; //montoMax
    
    /**
     * Constructor de la clase Cotizacion
     * @param numFloors
     * @param numDorms
     * @param numBaths
     * @param sector
     * @param income 
     * @param $entrada 
     */
    public Cotizacion(int numFloors, int numDorms, int numBaths, String sector, double income, double $entrada){
        this.numFloors = numFloors;
        this.numDorms  = numDorms;
        this.numBaths  = numBaths;
        this.sector    = sector;
        this.income    = income;
        this.$entrada = $entrada;
    }
    
    /**
     * Sobrecarga del constructor de la clase Cotizacion en caso de que se requiera un departamento
     * @param numDorms
     * @param numBaths
     * @param sector
     * @param income 
     * @param $entrada 
     */
    public Cotizacion(int numDorms, int numBaths, String sector, double income,double $entrada){
        this.numDorms  = numDorms;
        this.numBaths  = numBaths;
        this.sector    = sector;
        this.income    = income;
        this.$entrada = $entrada;
    }
    
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }
    
    public void setEmail(String email){
        this.email = email;
    }
    
    public String getEmail(){
        return email;
    }
    
    public void setNumFloors(int numFloors){
        this.numFloors = numFloors;
    }
    
    public int getNumFloors(){
        return numFloors;
    }
    
    public void setNumDorms(int numDorms){
        this.numDorms = numDorms;
    }
    
    public int getNumDorms(){
        return numDorms;
    }
    
    public void setNumBaths(int numBaths){
        this.numBaths = numBaths;
    }
    
    public int getNumBaths(){
        return numBaths;
    }
    
    public void setWantsHouse(boolean house){
        this.house = house;
    }
    
    public boolean getWantsHouse() {
        return house;
    }
    
    public void setWantsDep(boolean dep){
        this.dep = dep;
    }
    
    public boolean getWantsDep() {
        return dep;
    }
    
    public double getIncome(){
        return income;
    }
    
    public double getEntrada(){
        return $entrada;
    }
}
