package inmobiliaria;

import Modelos.Dep;
import Modelos.House;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Angellina
 */
public class Urbanization implements Serializable{
    //atributos
    private String name;
    private String sector;
    private ArrayList<Etapa> etapas;
    private ArrayList<House> houses;
    private ArrayList<Dep> apartments;
    
    public Urbanization(String name, String sector){ //para crear una urbanizacion y despues agregarle los datos
        this.name = name;
        this.sector = sector;
    }

    public void setName(String name){
        this.name = name;
    }
    
    public String getName(){
        return name;
    }
    
    public void setHouses(ArrayList<House> houses){
        this.houses = houses;
    }
   
    
    /**
     * 
     * @param sector 
     */
    public void setSector(String sector){
        this.sector = sector;
    }

    public String getSector(){
        return sector;
    }
    
    public ArrayList<Etapa> getEtapas(){
        return this.etapas;
    }
    
    public ArrayList<House> getHouses(){
        return this.houses;
    }
    
    public ArrayList<Dep> getDepartments(){
        return this.apartments;
    }
    public void setDeps(ArrayList<Dep> deps){
        this.apartments= deps;
    }
    public String toString(){
        String a,e,d,h;
        a = "Urbanización "+name+"\n Sector: "+sector;
        if(etapas != null){
            e = "Etapas: ";
            for(Etapa et:etapas){
                e.concat(" "+et.getNombre());
                a.concat("\n"+e);
            }
        }
        if(houses != null){
            h = "Modelos de casas:";
            for(House hou:houses){
                h.concat(" "+hou.getModel());
                a.concat("\n"+h);
            }    
        }
        if(apartments != null){
            d = "Modelos de departamentos: ";
            for(Dep de:apartments){
                d.concat(" "+de.getModel());
                a.concat("\n"+d);
            }
        }
        return a;
    }
    
    
    
    
   
    
   
}
