/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personas;

import java.util.ArrayList;

/**
 *
 * @author Angellina
 */
public class Admin{
    private String user; //para poder editar la inmobiliaria
    private String password;
    
    
    public void setUser(String user){
        this.user = user;
    }
    
    public String getUser(){
        return user;
    }
    
    public void setPassword(String password){
        this.password = password;
    }
    
    public String getPassword(){
        return password;
    }
    
    public Admin(String user, String password){
        this.user = user;
        this.password = password;
    }
    
}
   
