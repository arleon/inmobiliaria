/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import Modelos.Dep;
import Modelos.Home;
import Modelos.House;
import email.Email;
import email.Sender;
import inmobiliaria.Cotizacion;
import inmobiliaria.Etapa;
import inmobiliaria.Info;
import inmobiliaria.Mz;
import inmobiliaria.Urbanization;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import personas.Admin;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import inmobiliaria.Lote;
import java.io.IOException;

/**
 *
 * @author Angellina
 */
public class PrincipalInmobiliaria {

    public static final int PLAZO_CREDITO = 15;
    public static final double TASA_INTERES = 7.9;
    public static final int PORCENTAJE_MENSUAL = 40;

    static Scanner sc = new Scanner(System.in);
    static PrincipalInmobiliaria principal = new PrincipalInmobiliaria();
    private static ArrayList<Admin> admins = new ArrayList<>();
    private static ArrayList<Urbanization> urbanizations = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        //Llamada al menú Inicial
        Urbanization urbani = new Urbanization("La joya","La aurora");
        urbanizations.add(urbani);
        Admin a1 = new Admin("null", "pointer");
        admins.add(a1);
        ArrayList<House> casas = new ArrayList<>();
        casas.add(new House("Alma",3,2,70.4,65000.99,2,1,true));
        casas.add(new House("Antonella",3,2,70.4,60000.99,2,1,true));
        casas.add(new House("Valentina",3,2,70.4,75000.99,2,1,true));
        casas.add(new House("Eva",3,2,70.4,57000.99,2,1,true));
        casas.add(new House("Nadir",3,2,70.4,50000.99,2,1,true));
        urbani.setHouses(casas);
        ArrayList<Dep> depa = new ArrayList<>();
        depa.add(new Dep("Lila",3,2,68.3,74000,true,false,true));
        depa.add(new Dep("Mia",2,1,60.2,66000,true,true,true));
        depa.add(new Dep("Amara",3,3,83.8,86000,true,true,true));
        depa.add(new Dep("Emma",3,2,72.2,78000,false,false,true));
        depa.add(new Dep("Anya",2,2,72.1,75000,true,false,false));
        depa.add(new Dep("Alba",1,1,51.2,52000,true,false,false));
        urbani.setDeps(depa);
        Info.writeInfo(urbanizations);
        ArrayList<Urbanization> urbs= Info.readInfo();
        //System.out.println(urbs);
        principal.menuInicial();
    }

    /**
     * Menú inicial del sistema "Tu casa ya".
     * @throws IOException 
     */
    public void menuInicial() throws IOException {
        String opcion = "";
        while (!opcion.equals("3")) {
            System.out.println("Bienvenido al sistema de \"Tu casa ya\"");
            System.out.println("¿Qué desea hacer?");
            System.out.println("1.- Cotizar casas");
            System.out.println("2.- Entrar al sistema de inmobiliaria");
            System.out.println("3.- Salir");
            System.out.println("Ingrese opcion: ");
            opcion = sc.nextLine();
            switch (opcion) {
                case "1":
                    principal.cotizar();
                    break;
                case "2":
                    System.out.println("Es usuario registrado? (S) o (N)");
                    String resp1 = sc.nextLine();
                    if (resp1.equalsIgnoreCase("N")) {
                        principal.registrarNuevoUsuario();
                        System.out.println("Registro exitoso. Por favor inicie sesión.");
                    }
                    System.out.println("Plataforma de inicio de sesión");
                    principal.login();
                    break;
                case "3":
                    break;
                default:
                    System.out.println("Opción no válida, ingrese nuevamente");
            }
        }
        System.out.println("Gracias por usar el sistema, hasta pronto!");
        sc.close();
    }

    //OPCION 1 MENU INICIAL
    /**
     * Opción 1 de sistema, permite cotizar a un potencial cliente cotizar casa o departamentos.
     * @throws IOException 
     */
    public void cotizar() throws IOException {
        System.out.println("Hola! Bienvenido/a al sistema de la inmobiliaria \"Tu casa ya!\".\nMi nombre es Pangie, y el tuyo?");
        String clientName = sc.nextLine();
        System.out.println("Mucho gusto " + (clientName.split(" "))[0] + ". Cuál es tu email?");
        String clientEmail = sc.nextLine();
        boolean verificadorEmail = verificarEmail(clientEmail);
        while (verificadorEmail != true) {
            System.out.println("Email no válido. Ingresa tu email en formato user@example.com");
            clientEmail = sc.nextLine();
            verificadorEmail = verificarEmail(clientEmail);
        }
        System.out.println("Gracias. Ahora procederé a realizar unas preguntas acerca de ti.");
        Cotizacion clientCotizacion = principal.preguntarDatos();
        clientCotizacion.setName(clientName);
        clientCotizacion.setEmail(clientEmail);
        principal.mostrarOpciones(clientCotizacion);
        
        
        

    }

    /**
     * Verifica si el email ingresado por el usuario es válido o no
     * @param email email ingresado
     * @return true si el email es valido o false si no es valido
     */
    public boolean verificarEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
                + "[a-zA-Z0-9_+&*-]+)*@"
                + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
                + "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null) {
            return false;
        }
        boolean matched = pat.matcher(email).matches();
        return matched;
    }

    /**
     * Arma la cotización en base a las preferencias del cliente
     * @return Cotización dependiendo si el usuario solicita un departamento o una casa
     */
    public Cotizacion preguntarDatos() {
        System.out.println("¿Estás buscando una casa o un departamento?");
        String busqueda = sc.nextLine();
        while (!busqueda.equalsIgnoreCase("departamento") & !busqueda.equalsIgnoreCase("casa")) {
            System.out.println("Disculpa, no te entiendo. ¿estás buscando una casa o un departamento?");
            busqueda = sc.nextLine();
        }
        Cotizacion c1;
        if (busqueda.equalsIgnoreCase("casa")) {
            boolean wantsHouse = true;
            boolean wantsDep = false;
            c1 = principal.preguntarDatosHouse();
            c1.setWantsHouse(wantsHouse);
            c1.setWantsDep(wantsDep);
            return c1;
        }
        if (busqueda.equalsIgnoreCase("departamento")) {
            boolean wantsHouse = false;
            boolean wantsDep = true;
            c1 = principal.preguntarDatosDep();
            c1.setWantsHouse(wantsHouse);
            c1.setWantsDep(wantsDep);
            return c1;
        }
        return null;
    }

    /**
     * Al requerir el cliente una casa, se procede a preguntar acerca de sus prefencias
     * @return Cotización con los datos obtenidos
     */
    public Cotizacion preguntarDatosHouse() {
        System.out.println("Comencemos. ¿Te gustaría una casa de uno (1) o dos (2) pisos? Puedes escribir \"0\" si no tienes preferencia alguna.");
        int numFloors = sc.nextInt();
        System.out.println("Perfecto. ¿Cuántas habitaciones? Puedes escribir \"0\" si no tienes preferencia alguna.");
        int numDorms = sc.nextInt();
        System.out.println("Ya casi llegamos a la mitad. ¿Cuántos baños deseas? Puedes escribir \"0\" si no tienes preferencia alguna.");
        int numBaths = sc.nextInt();
        System.out.println("¡Listo! ¿En qué sector deseas que se encuentre tu casa? Puedes escribir \"0\" si no tienes preferencia alguna.");
        String sector = sc.nextLine();
        System.out.println("Ya casi acabamos. Necesito saber tus ingresos mensuales, pero primero: ¿Estas casado? (s) o (n).");
        String estadoMarital = sc.nextLine();
        double income;
        if (estadoMarital.equalsIgnoreCase("s")) {
            System.out.println("¿Cuál es tu ingreso mensual?");
            double incomeCliente = sc.nextDouble();
            System.out.println("¿Cuál es el ingreso de tu cónyuge?");
            double incomePartner = sc.nextDouble();
            income = incomeCliente + incomePartner;
        } else {
            System.out.println("¿Cuál es tu ingreso mensual?");
            income = sc.nextDouble();
        }
        System.out.println("Última pregunta: ¿Cuál es el valor mínimo de entrada que puedes ofrecer?");
        double $entrada = sc.nextDouble();

        Cotizacion c = new Cotizacion(numFloors, numDorms, numBaths, sector, income, $entrada);
        System.out.println("¡Genial! Muchas gracias por responder. Tus datos ya se encuentran en nuestro sistema.");
        return c;
    }

    /**
     * Al conocer los ingresos y valor de entrada ingresador por el usuario, muestra las opciones de casa o departamento por pantalla
     * @param cotizacion Cotización que contiene información del cliente y sus prefencias de casa ingresadas
     * @throws IOException 
     */
    public void mostrarOpciones(Cotizacion cotizacion) throws IOException {
        double prestamoMaximo = calculoMaxPrestamo(cotizacion.getIncome());
        System.out.println("Su monto máximo para el préstamo (sin incluir la entrada) es de " + prestamoMaximo + " dolares.");
        double dineroDisponible = prestamoMaximo + cotizacion.getEntrada();
        System.out.println("En general, usted puede pagar una casa de hasta " + dineroDisponible + " dolares.");

        ArrayList<House> casasMostrar = new ArrayList<>();
        if (cotizacion.getWantsHouse() == true) {
            ArrayList<House> casasPagar = calculoCotizadorHouse(dineroDisponible, PrincipalInmobiliaria.urbanizations); //Casas que estan dentro de su presupuesto
            ArrayList<House> casasCumplen = opcionesHouse(cotizacion, casasPagar); //Casas dentro del presupuesto y que cumplen con lo que quiere el usuario
            if (casasCumplen.size() != 5) {
                if (casasCumplen.size() < 5) {
                    casasMostrar = casasCumplen;
                    for (int i = 0; i < casasPagar.size(); i++) {
                        if (!casasMostrar.contains(casasPagar.get(i)) & casasMostrar.size() <= 5) {
                            casasMostrar.add(casasPagar.get(i));
                        }
                    }
                }
                if (casasCumplen.size() > 5) {
                    for (int j = 0; j < 5; j++) {
                        casasMostrar.add(casasCumplen.get(j));
                    }
                }
            }
            else {
                casasMostrar = casasCumplen;
            }
            int hnum = 1;
            for(House h: casasMostrar){
                System.out.println("Opción "+hnum);
                System.out.println(h.showInfo(h.getModel()));
                hnum += 1;
            }
        }
        
        ArrayList<Dep> depsMostrar = new ArrayList<>();
        if (cotizacion.getWantsDep() == true) {
            ArrayList<Dep> depsPagar = calculoCotizadorDep(dineroDisponible, PrincipalInmobiliaria.urbanizations);
            ArrayList<Dep> depsCumplen = opcionesDep(cotizacion, depsPagar);
            if (depsCumplen.size() != 5) {
                if (depsCumplen.size() < 5) {
                    depsMostrar = depsCumplen;
                    for (int i = 0; i < depsPagar.size(); i++) {
                        if (!depsMostrar.contains(depsPagar.get(i)) & depsMostrar.size() <= 5) {
                            depsMostrar.add(depsPagar.get(i));
                        }
                    }
                }
                else{
                    for (int j = 0; j < 5; j++) {
                        depsMostrar.add(depsCumplen.get(j));
                    }
                }
            } 
            else {
                depsMostrar = depsCumplen;
            }
            int depnum = 1;
            for(Dep d: depsMostrar){
                System.out.println("Opción "+depnum);
                d.showInfo(d.getModel());
                depnum += 1;
            }
        }
        System.out.println("¿Desea más información acerca de algún modelo? Se la enviaremos a su correo");
        String opCasaEnviar = "";
        while(principal.verificarOpciones(opCasaEnviar) == null){
            opCasaEnviar = sc.nextLine();
            int[] opCasasNum = principal.verificarOpciones(opCasaEnviar);
            if(opCasasNum != null){
                if(cotizacion.getWantsDep() == true){
                    for(int numDep : opCasasNum){
                        Dep departamento1 = depsMostrar.get(numDep-1);
                        String archivoDep = departamento1.DepEmail(departamento1);
                        String ruta = "C:\\Users\\Andres\\Documents\\inmobiliaria\\archivoHouse.txt";
                        //Email e= new Email("C://documentos//carpeta//archivoDep.txt","Información del modelo: "+depart.getModel(),"archivoDep.txt",cotizacion.getEmail(), "MENSAJE");
                        Email e= new Email("C:C:\\Users\\Andres\\Documents\\inmobiliaria\\archivoHouse.txt"+archivoDep,"Información del modelo: "+departamento1.getModel(),archivoDep,cotizacion.getEmail(), "Información de departamento");
                        Sender send= new Sender();
                        send.SendMail(e);
                        
                    }
                }
                if(cotizacion.getWantsHouse() == true){
                    for(int numHou : opCasasNum){
                        House hou1 = casasMostrar.get(numHou-1);
                        String archivoHo = hou1.CasaEmail(hou1);
                        //String ruta = "C:\\Users\\pb\\Documents\\inmobiliaria\\archivoHouse.txt";
                        //Email em= new Email("C://documentos//carpeta//archivoHouse.txt","Información del modelo: "+hous.getModel(),archivoHo,cotizacion.getEmail(), "MENSAJE");
                        
                        Email em= new Email(archivoHo,
                                "Información del modelo","archivoHouse.txt",cotizacion.getEmail(), "Información");
                        Sender send= new Sender();
                        send.SendMail(em);
                    }
                    System.out.println("Listo, se ha enviado la información a tu correo");
                }
                
            }
            else{
                
                System.out.println("Opciones ingresadas no válidas");
                
            }
        }

        
    }

    /**
     * Verifica si las opciones ingresadas por el usuario cumplen con el formato: "opcion1,opcion2,...,opcion5" y son número entre el 1 y el 5
     * @param opcion String ingresado por cliente
     * @return Arreglo de números ingresados
     */
    public int[] verificarOpciones(String opcion){
        int[] opcionesnum;
          
        if((opcion.equals("1"))||(opcion.equals("2"))||(opcion.equals("3"))||(opcion.equals("4"))||(opcion.equals("5"))){
            opcionesnum = new int[1];
            int op = Integer.parseInt(opcion);
            opcionesnum[0] = op;
        }
        
        else if(opcion.contains(",")){
            String[] opciones = opcion.split(",");
            opcionesnum = new int[opciones.length];
            int i = 0;
            for(String o:opciones){
                if((o.equals("1"))||(o.equals("2"))||(o.equals("3"))||(o.equals("4"))||(o.equals("5"))){
                    int op = Integer.parseInt(o);
                    opcionesnum[i]=op;
                    i++;
                }
                else{
                    opcionesnum = null;
                    
                    break;
                }
            }
        }
        
        else{
            opcionesnum = null;
        }
        
        return opcionesnum;
    }
    
    
    /**
     * Calcula el máximo prestamo que puede realizar un usuario en un periodo de
     * 15 años.
     *
     * @param income Ingresos del usuario
     * @return Valor de prestamo máximo de usuario
     */
    public double calculoMaxPrestamo(double income) {
        double cuotaMax = (PORCENTAJE_MENSUAL * income)/100;
        double periodoFijo = PLAZO_CREDITO * 12;
        double tipoI = TASA_INTERES / 1200;
        double denom = (tipoI * (Math.pow((1 + tipoI), periodoFijo)));
        double num = (Math.pow((1 + tipoI), periodoFijo)) - 1;
        double maxPrestamo = cuotaMax * (num / denom);
        return maxPrestamo;
    }

    /**
     * Calculo de cotizador para las casas, determina si alguno de los modelos de casa disponibles en urbanizaciones 
     * las puede pagar el cliente
     *
     * @param dineroDisponible Prestamo máximo que puede realizar el cliente más el precio de entrada fijado
     * @param urbanizations Urbanizaciones totales disponibles
     * @return Casas disponibles que el usuario las puede pagar dependiendo sus ingresos
     */
    public ArrayList<House> calculoCotizadorHouse(double dineroDisponible, ArrayList<Urbanization> urbanizations) {
        ArrayList<House> housesDisp = new ArrayList<>();
        for (Urbanization u : urbanizations) {
            for (House h : u.getHouses()) {
                double precio = h.getPrice();
                //if ((precio <= dineroDisponible & !housesDisp.contains(h))) {
                if ((precio <= dineroDisponible)) {  
                    housesDisp.add(h);
                }
            }
        }
        Collections.sort(housesDisp);
        return housesDisp;
    }

    /**
     * Calculo de cotizador para departamentos, determina si alguno de los modelos de departamentos disponibles en urbanizaciones 
     * las puede pagar el cliente
     * @param dineroDisponible Prestamo máximo que puede realizar el cliente más el precio de entrada fijado
     * @param urbanizations Urbanizaciones totales disponibles
     * @return Departamentos disponibles que el usuario las puede pagar dependiendo sus ingresos
     */
    public ArrayList<Dep> calculoCotizadorDep(double dineroDisponible, ArrayList<Urbanization> urbanizations) {
        ArrayList<Dep> depDisp = new ArrayList<>();
        for (Urbanization u : urbanizations) {
            for (Dep d : u.getDepartments()) {
                double precio = d.getPrice();
                //if ((precio <= dineroDisponible & !depDisp.contains(d))) {
                if ((precio <= dineroDisponible)) {
                    depDisp.add(d);
                }
            }
        }
        Collections.sort(depDisp);
        return depDisp;
    }

    /**
     * De un array de casas escoge los que tengan las opciones de número de
     * plantas, cuartos y baños que el usario haya seleccionado
     *
     * @param c Cotización realizada por usuario con sus preferencias
     * @param housesDisp Casas disponibles que el usuario puede pagar según sus
     * ingresos y el valor de entrada
     * @return Casas que cumplen con los gustos de usuario y que las pueda pagas
     */
    public ArrayList<House> opcionesHouse(Cotizacion c, ArrayList<House> housesDisp) {
        ArrayList<House> housesOp = new ArrayList<>();
        for (House h : housesDisp) {
            //if (!housesOp.contains(h)) {
                if ((c.getNumBaths() == h.getnoBaths()) && (c.getNumFloors() == h.getnumFloors()) && (c.getNumDorms() == h.getnoDorms())) {
                    housesOp.add(h);
                }
            //}
        }
        return housesOp;
    }

    /**
     * De un array de departamentos escoge los que tengan las opciones de número
     * de cuartos y baños que el usario haya seleccionado
     *
     * @param c Cotización del usuario con su número de baños y cuartos
     * @param depsDisp Departamentos que se encuentren disponibles para el
     * usuario según sus ingresos y el valor de entrada
     * @return Departamentos que el usuario puede pagar y que se encuentren
     * acorde a sus gustos
     */
    public ArrayList<Dep> opcionesDep(Cotizacion c, ArrayList<Dep> depsDisp) {
        ArrayList<Dep> depsOp = new ArrayList<>();
        for (Dep d : depsDisp) {
            //if (!depsOp.contains(d)) {
                if ((c.getNumBaths() == d.getnoBaths()) & (c.getNumDorms() == d.getnoDorms())) {
                    depsOp.add(d);
                //}
            }
        }
        return depsOp;
    }

    /**
     * Al requerir el cliente un departamento, se procede a preguntar acerca de sus prefencias
     * @return Cotización dependiendo de opciones ingresadas por cliente
     */
    public Cotizacion preguntarDatosDep() {
        System.out.println("Perfecto. ¿Cuántas habitaciones? Puedes escribir \"0\" si no tienes preferencia alguna.");
        int numDorms = sc.nextInt();
        System.out.println("Ya casi llegamos a la mitad. ¿Cuántos baños deseas? Puedes escribir \"0\" si no tienes preferencia alguna.");
        int numBaths = sc.nextInt();
        System.out.println("¡Listo! ¿En qué sector deseas que esté tu casa? Puedes escribir \"0\" si no tienes preferencia alguna.");
        String sector = sc.nextLine();
        System.out.println("Ya casi acabamos. Necesito saber tus ingresos mensuales, pero primero: Eres casado? (s) o (n).");
        String estadoMarital = sc.nextLine();
        double income = 0.0;
        if (estadoMarital.equalsIgnoreCase("s")) {
            System.out.println("¿Cuál es tu ingreso mensual?");
            double incomeCliente = sc.nextDouble();
            System.out.println("¿Cuál es el ingreso de tu cónyuge?");
            double incomePartner = sc.nextDouble();
            income = incomeCliente + incomePartner;
        } else {
            System.out.println("¿Cuál es tu ingreso mensual?");
            income = sc.nextDouble();
        }
        System.out.println("Última pregunta: ¿Cuál es el valor mínimo de entrada que puedes ofrecer?");
        double $entrada = sc.nextDouble();

        Cotizacion c = new Cotizacion(numDorms, numBaths, sector, income, $entrada);
        System.out.println("¡Genial! Muchas gracias por responder. Tus datos ya se encuentran en nuestro sistema.");
        return c;
    }

    //OPCION 2 MENU INICIAL
    /**
     * Registra un nuevo usuario al sistema de cotizaciones
     */
    public void registrarNuevoUsuario() {
        System.out.println("Ingrese un nombre de usuario:");
        String user = sc.nextLine();
        while (PrincipalInmobiliaria.buscarAdmin(user) != false) {
            System.out.println("Nombre de usuario ya existente. Por favor ingrese uno nuevo: ");
            user = sc.nextLine();
        }
        System.out.println("Nombre de usuario disponible.\nIngrese una contraseña que posea:\n -Mínimo 6 caractéres\n -Al menos una letra\n -Al menos un número\n -Tenga en cuenta mayúsculas y minúsculas");
        String password = sc.nextLine();
        while (PrincipalInmobiliaria.validarContrasena(password) == false) {
            System.out.println("Contraseña no válida. Intente de nuevo.");
            password = sc.nextLine();
        }
        Admin a1 = new Admin(user, password);
        admins.add(a1);
    }

    /**
     * Verifica si el nombre de usuario ingresado ya ha sido usado antes
     * @param user Nombre de usuario ingresado
     * @return true si el nombre existe, false si no existe y el usuario lo puede ocupar
     */
    public static boolean buscarAdmin(String user) { 
        for (Admin a : PrincipalInmobiliaria.admins) {
            if (a.getUser().equalsIgnoreCase(user)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determina si la contraseña ingresada es valida
     * @param password Contraseña ingresada por usuario
     * @return true si contraseña es valida, false si no lo es
     */
    public static boolean validarContrasena(String password) {
        if (password.length() < 6) {
            return false;
        } else {
            int contLetra = 0, contNumero = 0;
            char clave;
            for (int i = 0; i < password.length(); i++) {
                clave = password.charAt(i);
                if (Character.isLetter(clave)) {
                    contLetra++;
                } else if (Character.isDigit(clave)) {
                    contNumero++;
                }
            }
            if (contLetra != 0 & contNumero != 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Inicia sesión con el nombre de usuario y contraseña ingresada
     */
    public static void login() {
        System.out.println("Ingrese su nombre de usuario:");
        String user = sc.nextLine();
        while (PrincipalInmobiliaria.buscarAdmin(user) == false) {
            System.out.println("Nombre de usuario no existente. Por favor intente nuevamente. ");
            user = sc.nextLine();
        }
        System.out.println("Ingrese su contraseña: ");
        String password = sc.nextLine();
        while (PrincipalInmobiliaria.comprobarContrasena(password) == false) {
            System.out.println("Usuario y contraseña no coinciden. Ingrese nuevamente la contraseña:");
            password = sc.nextLine();
        }
        principal.menuEdiciones1();
    }

    /**
     * Verifica si la contraseña ingresada al iniciar sesión es la misma que la ingresada al momento de registrarse
     * @param password Contraseña ingresda por usuario
     * @return true si contraseña es correcta
     */
    public static boolean comprobarContrasena(String password) {
        for (Admin a : PrincipalInmobiliaria.admins) {
            if (a.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Menú que permite accedo a las ediciones de la inmobiliaria
     */
    public static void menuEdiciones1() {
        String opcion = "";
        while (!opcion.equals("6")) {
            System.out.println("Bienvenido al editor de la inmobiliaria.");
            System.out.println("¿Qué desea hacer?");
            System.out.println("1.- Urbanizaciones");
            System.out.println("2.- Etapas");
            System.out.println("3.- Casas/Departamentos modelo");
            System.out.println("4.- Manzanas");
            System.out.println("5.- Lotes");
            System.out.println("6.- Regresar");
            System.out.println("Ingrese opcion: ");
            opcion = sc.nextLine();
            switch (opcion) {
                case "1":
                    String opcion1 = "";
                    while (!opcion1.equalsIgnoreCase("4")) {
                        System.out.println("Menú urbanizaciones");
                        System.out.println("Qué desea hacer?");
                        System.out.println("1.- Añadir urbanizacion");
                        System.out.println("2.- Consultar urbanizacion");
                        System.out.println("3.- Eliminar urbanizacion");
                        System.out.println("4.- Regresar");
                        System.out.println("Ingrese opcion: ");
                        opcion1 = sc.nextLine();
                        switch (opcion1) {
                            case "1":
                                PrincipalInmobiliaria.addUrbanization();
                                break;
                            case "2":
                                PrincipalInmobiliaria.showUrbanization();
                                break;
                            case "3":
                                PrincipalInmobiliaria.deleteUrbanization();
                        }

                    }

                    break;

                case "2":
                    System.out.println("Menú etapas");
                    System.out.println("Qué desea hacer?");
                    System.out.println("1.- Añadir etapas");
                    System.out.println("2.- Consultar etapas");
                    System.out.println("3.- Eliminar etapas");
                    System.out.println("4.- Regresar");

                    String opcion2 = "";
                    while (!opcion2.equalsIgnoreCase("4")) {
                        opcion2 = sc.nextLine();
                        switch (opcion2) {
                            case "1":
                                PrincipalInmobiliaria.addEtapa();
                                break;
                            case "2":
                                PrincipalInmobiliaria.showEtapa();
                                break;
                            case "3":
                                PrincipalInmobiliaria.deleteEtapa();
                                break;                                
                        }
                    }
                    break;
                case "3":
                    System.out.println("Menú casas/departamentos");
                    System.out.println("Qué desea hacer?");
                    System.out.println("1.- Añadir casas");
                    System.out.println("2.- Consultar casas");
                    System.out.println("3.- Eliminar casas");
                    System.out.println("4.- Añadir departamentos");
                    System.out.println("5.- Consultar departamentos");
                    System.out.println("6.- Eliminar departamentos");
                    System.out.println("7.- Regresar");

                    String opcion3 = "";
                    while (!opcion3.equalsIgnoreCase("4")) {
                        opcion2 = sc.nextLine();
                        switch (opcion2) {
                            case "1":
                                PrincipalInmobiliaria.addHouse();
                                break;
                            case "2":
                                PrincipalInmobiliaria.showHouse();
                                break;
                            case "3":
                                PrincipalInmobiliaria.deleteHouse();
                                break;
                            case "4":
                                PrincipalInmobiliaria.addDep();
                                break;
                            case "5":
                                PrincipalInmobiliaria.showDep();
                                break;
                            case "6":
                                PrincipalInmobiliaria.deleteDep();
                                break;
                        }
                    }
                    break;
                case "4":
                    System.out.println("Menú manzanas");
                    System.out.println("Qué desea hacer?");
                    System.out.println("1.- Añadir manzanas");
                    System.out.println("2.- Consultar manzanas");
                    System.out.println("3.- Eliminar manzanas");
                    System.out.println("4.- Regresar");

                    String opcion4 = "";
                    while (!opcion4.equalsIgnoreCase("4")) {
                        opcion4 = sc.nextLine();
                        switch (opcion4) {
                            case "1":
                                PrincipalInmobiliaria.addMz();
                                break;
                            case "2":
                                PrincipalInmobiliaria.showMz();
                                break;
                            case "3":
                                PrincipalInmobiliaria.deleteMz();
                                break;                                
                        }
                    }
                    break;
                case "5":
                    System.out.println("Menú lotes");
                    System.out.println("Qué desea hacer?");
                    System.out.println("1.- Añadir lote");
                    System.out.println("2.- Consultar lote");
                    System.out.println("3.- Eliminar lote");
                    System.out.println("4.- Regresar");

                    String opcion5 = "";
                    while (!opcion5.equalsIgnoreCase("4")) {
                        opcion5 = sc.nextLine();
                        switch (opcion5) {
                            case "1":
                                PrincipalInmobiliaria.addLote();
                                break;
                            case "2":
                                PrincipalInmobiliaria.showLote();
                                break;
                            case "3":
                                PrincipalInmobiliaria.deleteLote();
                                break;                                
                        }
                    }
                    break;
                case "":
                    break;
                default:
                    System.out.println("Opción no válida, ingrese nuevamente");
            }
        }
    }

    /**
     * Verifica si la urbanización ingresada existe 
     * @param s Nombre de la urbanización
     * @param arr ArrayList que contiene a todas las urbanizaciones que maneja la inmobiliaria
     * @return 
     */
    public static Urbanization verificarUrb(String s, ArrayList<Urbanization> arr) {
        for (Urbanization u : arr) {
            if (u.getName().equals(s)) {
                return u;
            }
        }
        return null;
    }

    /**
     * Añade una nueva urbanizacion a la inmobiliaria.
     */
    public static void addUrbanization() {
        System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        System.out.println("Ingrese la dirección de la urbanización: ");
        String sectorUrb = sc.nextLine();
        urbanizations = (ArrayList<Urbanization>) Info.readInfo(); //readInfo devuelve un List, no un ArrayList
        if (verificarUrb(nameUrb, urbanizations) == null) {
            Urbanization urb1 = new Urbanization(nameUrb, sectorUrb);
            urbanizations.add(urb1);
            Info.writeInfo(urbanizations);
            System.out.println("La urbanzación ha sido añadida con éxito");

        } else {
            System.out.println("La urbanización ya existe");
        }
    }

    /**
     * Permite al usuario ingresar el nombre de una urbanización y obtener información.
     */
    public static void showUrbanization() {
        System.out.println("Ingrese nombre de la urbanización que desea consultar: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if (urb != null) {
            System.out.println(urb.toString());//override esto;                        ;
        } else {
            System.out.println("La urbanizacion no existe!");
        }
    }

    /**
     * Elimina una urbanización del ArrayList urbanizations.
     */
    public static void deleteUrbanization() {
        System.out.println("Ingrese nombre de la urbanización que desea borrar: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if (urb != null) {
            urbanizations.remove(urb);
            System.out.println("La urbanización ha sido eliminada con éxito");
        } else {
            System.out.println("La urbanización no existe");
        }
    }

    //ETAPAS 
    /**
     * Verifica si la etapa ingresada por el usuario existe.
     * @param s Nombre de la etapa ingresada por usuario
     * @param urb ArrayLista que contiene todas las urbanizaciones
     * @return 
     */
    public static Etapa verificarEtapa(String s, Urbanization urb) {
        for (Etapa etap : urb.getEtapas()) {
            if (etap.getNombre().equals(s)) {
                return etap;
            }
        }
        return null;
    }

    /**
     * Añade una nueva etapa a la urbanización escogida.
     */
    public static void addEtapa() {
        System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if (verificarUrb(nameUrb, urbanizations) == null) {
            System.out.println("La urbanizacion no existe");
        } else {
            System.out.println("Ingrese nombre de la nueva Etapa: ");
            String nameEtapa = sc.nextLine();
            Etapa etapa = verificarEtapa(nameEtapa, urb);
            if (etapa != null) {
                System.out.println("La etapa ya existe");
            } else {
                System.out.println("Ingrese el numero de manzanas de la etapa: ");
                int numMz = sc.nextInt();

                urb.getEtapas().add(new Etapa(nameEtapa, numMz));
            }

        }

    }

    /**
     * Muestra información acerca de una etapa existente en la urbanización ingresada.
     */
    public static void showEtapa() {
        System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if (verificarUrb(nameUrb, urbanizations) == null) {
            System.out.println("La urbanizacion no existe");
        } else {
            System.out.println("Ingrese nombre de la Etapa que desea consultar: ");
            String nameEtapa = sc.nextLine();
            Etapa etapa = verificarEtapa(nameEtapa, urb);
            if (etapa != null) {
                etapa.toString();;
            } else {
                System.out.println("La etapa no existe!");
            }

        }

    }

    /**
     * Elimina una etapa de una urbanización ingresada
     */
    public static void deleteEtapa() {
        System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if (verificarUrb(nameUrb, urbanizations) == null) {
            System.out.println("La urbanizacion no existe");
        } else {
            System.out.println("Ingrese nombre de la Etapa que desea eliminar: ");
            String nameEtapa = sc.nextLine();
            Etapa etapa = verificarEtapa(nameEtapa, urb);
            if (etapa != null) {
                urb.getEtapas().remove(etapa);;
                System.out.println("La etapa fue eliminada con éxito");
            } else {
                System.out.println("La etapa no existe!");
            }
        }
    }

    //MANZANAS
    /**
     * Verifica si existe una manzana con el nombre ingresado
     *
     * @param etap Etapa que contenga a manzana por verificar
     * @param numMz Número de manzana
     * @return Si existe la manzana devolverá una manzana, caso contrario
     * devuelve null
     */
    public static Mz verificarMz(Etapa etap, int numMz) {
        for (Mz maz : etap.getManzanas()) {
            if (maz.getnumMz() == numMz) {
                return maz;
            }
        }
        return null;
    }

    /**
     * Añade una manzana a una etapa ingresada por el usuario
     */
    public static void addMz() {
        System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if (verificarUrb(nameUrb, urbanizations) == null) {
            System.out.println("La urbanizacion no existe");
        } else {
            System.out.println("Ingrese nombre de la Etapa: ");
            String nameEtapa = sc.nextLine();
            Etapa etapa = verificarEtapa(nameEtapa, urb);
            if (etapa == null) {
                System.out.println("La etapa ingresada no existe");
            } else {
                ArrayList<Mz> manzanas = etapa.getManzanas();
                int numMz = manzanas.size()+1;
                Mz maz = verificarMz(etapa, numMz);//verificar manzana
                if (maz != null) {
                    System.out.println("La manzana ingresada ya existe. ");
                } else {
                    System.out.println("Ingrese el número de lotes: ");
                    int numLote = sc.nextInt();
                    etapa.getManzanas().add(new Mz(numLote, numMz));
                    System.out.println("Manzana añadida con exito");

                }
            }
        }
    }

    /**
     * Consulta una manzana ingresando el nombre de la etapa y de la
     * urbanización
     */
    public static void showMz() {
        System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if (verificarUrb(nameUrb, urbanizations) == null) {
            System.out.println("La urbanizacion no existe");
        } else {
            System.out.println("Ingrese nombre de la Etapa: ");
            String nameEtapa = sc.nextLine();
            Etapa etapa = verificarEtapa(nameEtapa, urb);
            if (etapa == null) {
                System.out.println("La etapa no existe!");
            } else {
                //(????
                System.out.println("Ingrese el número de manzana: ");
                int numMz = sc.nextInt();
                Mz maz = verificarMz(etapa, numMz);
                if (maz == null) {
                    System.out.println("La manzana ingresada no existe.");
                } else {
                    //toString manzana
                }
            }
        }
    }

    /**
     * Eliminar una manzana de una etapa ingresada
     */
    public static void deleteMz() {
        System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if (verificarUrb(nameUrb, urbanizations) == null) {
            System.out.println("La urbanizacion no existe");
        } else {
            System.out.println("Ingrese nombre de la Etapa: ");
            String nameEtapa = sc.nextLine();
            Etapa etapa = verificarEtapa(nameEtapa, urb);
            if (etapa == null) {
                System.out.println("La etapa no existe");

            } else {
                System.out.println("Ingrese el número de la manzana: ");
                int numMz = sc.nextInt();
                Mz maz = verificarMz(etapa, numMz);
                if (maz == null) {
                    System.out.println("La manzana no existe");
                } else {
                    etapa.getManzanas().remove(maz);
                }
            }
        }
    }
    
    /**
     * Verifica si el Lote ingresado existe
     * @param mz Número de manzana que contiene al lote
     * @param numLote Número de lote por verificar
     * @return Objeto tipo Lote
     */
    public static Lote verificarLote(Mz mz, int numLote){
        for(Lote lot : mz.getLotes()){
              if(lot.getnumLote() == numLote){
                    return lot;
                }  
            }
        return null;
    }
    
    /**
     * Añade un lote a la manzada ingresada.
     */
    public static void addLote(){
       System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if(verificarUrb(nameUrb, urbanizations)== null){
            System.out.println("La urbanizacion no existe");
        }
        else{
            System.out.println("Ingrese nombre de la Etapa: ");
            String nameEtapa = sc.nextLine();
            Etapa etapa = verificarEtapa(nameEtapa, urb);      
            if (etapa == null){
                System.out.println("La etapa ingresada no existe");
            }
            else{
                System.out.println("Ingrese el numero de manzana: ");
                int numMz = sc.nextInt(); 
                Mz maz = verificarMz(etapa,numMz);//verificar manzana
                if(maz != null){
                    System.out.println("La manzana ingresada ya existe. ");
                }
                else{
                    ArrayList<Lote> lotes = maz.getLotes();
                    if (lotes.size() >= maz.getnumLotes()){
                        System.out.println("Ya no se pueden ingresar más lotes en la manzana");
                    }
                    else{
                        System.out.println("Ingrese el tamaño en metros cuadrados, del lote: ");
                        int sizeLote = sc.nextInt();
                        lotes.add(new Lote((lotes.size()+1), maz.getnumMz(), sizeLote));
                        System.out.println("Lote añadido con exito");
                    }
                }
                
            }
        }
    } 
    
    /**
     * Muestra la información de un lote existente en la urbanización.
     */
    public static void showLote(){
       System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if(verificarUrb(nameUrb, urbanizations)== null){
            System.out.println("La urbanizacion no existe");
        }
        else{
            System.out.println("Ingrese nombre de la Etapa: ");
            String nameEtapa = sc.nextLine();
            Etapa etapa = verificarEtapa(nameEtapa, urb);      
            if (etapa == null){
                System.out.println("La etapa ingresada no existe");
            }
            else{
                System.out.println("Ingrese el numero de manzana: ");
                int numMz = sc.nextInt(); 
                Mz maz = verificarMz(etapa,numMz);//verificar manzana
                if(maz != null){
                    System.out.println("La manzana ingresada ya existe. ");
                }
                else{
                    System.out.println("Ingrese el número del lote que desea consultar: ");
                    int numLot = sc.nextInt();
                    Lote lot = verificarLote(maz,numLot);
                    if (lot==null){
                        System.out.println("El lote ingresado no existe");
                    }
                    else{
                        lot.toString();//Dont forget to override this method 
                    }
                }
                
            }
        }
    }
    
    /**
     * Elimina un lote existente.
     */
    public static void deleteLote(){
        System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if(verificarUrb(nameUrb, urbanizations)== null){
            System.out.println("La urbanizacion no existe");
        }
        else{
            System.out.println("Ingrese nombre de la Etapa: ");
            String nameEtapa = sc.nextLine();
            Etapa etapa = verificarEtapa(nameEtapa, urb);      
            if (etapa == null){
                System.out.println("La etapa ingresada no existe");
            }
            else{
                System.out.println("Ingrese el numero de manzana: ");
                int numMz = sc.nextInt(); 
                Mz maz = verificarMz(etapa,numMz);//verificar manzana
                if(maz != null){
                    System.out.println("La manzana ingresada ya existe. ");
                }
                else{
                    ArrayList<Lote> lotes = maz.getLotes();
                    System.out.println("Ingrese el número del lote que desea eliminar: ");
                    int numLot = sc.nextInt();
                    Lote lot = verificarLote(maz,numLot);
                    if (lot==null){
                        System.out.println("El lote ingresado no existe");
                    }
                    else{
                        lotes.remove(lot);
                        System.out.println("El lote se ha eliminado con éxito");
                    }
                }
                
            }
        }
    }
    
    /**
     * Verifica si existe una casa con el modelo ingresado.
     * @param urb Urbanización que contiene la casa
     * @param model Modelo de la casa por verificar
     * @return House si existe la casa
     */
    public static House verificarHouse(Urbanization urb, String model){
        if(urb.getHouses() != null){
            for (House h: urb.getHouses()){
                if (h.getModel()==model){
                    return h;
                }
            }
        }
        return null;
    }
    
    /**
     * Añade una casa a la urbanización ingresada.
     */
    public static void addHouse() {
        System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if (verificarUrb(nameUrb, urbanizations) == null) {
            System.out.println("La urbanizacion no existe");
        } 
        else {
            System.out.println("Ingrese nombre del nuevo modelo de casa: ");
            String model = sc.nextLine();
            House house = verificarHouse(urb, model);
            
            if (house != null) {
                System.out.println("Ese modelo ya existe");
            } else {
                
                System.out.println("Ingrese el número de dormitorios: ");
                int noDorms = sc.nextInt();
                System.out.println("Ingrese el número de baños: ");
                int noBaths = sc.nextInt();
                System.out.println("Ingrese el tamaño de la casa en metros cuadrados: ");
                double size = sc.nextDouble();
                System.out.println("Ingrese el precio: ");
                double precio = sc.nextDouble();
                System.out.println("Ingrese el número de plantas: ");
                int numFloor = sc.nextInt();
                System.out.println("Ingrese el número de cocinas: ");
                int numKit = sc.nextInt();
                String roomSN = "";
                boolean servRoom = true;
                while(roomSN.equals("")){
                    System.out.println("¿Posee cuarto de servicio?: S/N");
                    roomSN = sc.nextLine();
                    if(roomSN.toUpperCase().equals("S")){
                        servRoom = true;
                    }
                    else if(roomSN.toUpperCase().equals("N")){
                        servRoom = false;
                    }
                    else{
                        System.out.println("Opcion ingresada no válida.");
                        roomSN = "";
                    }
                }
                if(urb.getHouses() == null){
                    ArrayList<House> hSola = new ArrayList<>();
                    hSola.add(new House(model,noDorms,noBaths,size,precio,numFloor,numKit,servRoom));
                    urb.setHouses(hSola);
                    System.out.println("La casa se ha añadido con éxito");
                }
                else{
                urb.getHouses().add(new House(model,noDorms,noBaths,size,precio,numFloor,numKit,servRoom));
                System.out.println("La casa se ha añadido con éxito");
                }
            }

        }

    }

    /**
     * Muestra información acerca de una casa existente en la urbanización.
     */
    public static void showHouse() {
        System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if (verificarUrb(nameUrb, urbanizations) == null) {
            System.out.println("La urbanizacion no existe");
        } else {
            System.out.println("Ingrese nombre del modelo de casa que desea consultar: ");
            String model = sc.nextLine();
            House house = verificarHouse(urb, model);
            if (house != null) {
                house.toString();
            } else {
                System.out.println("La casa no existe!");
            }

        }

    }

    /**
     * Elimina una casa existente.
     */
    public static void deleteHouse() {
        System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if (verificarUrb(nameUrb, urbanizations) == null) {
            System.out.println("La urbanizacion no existe");
        } else {
            System.out.println("Ingrese nombre del modelo de casas que desea eliminar: ");
            String model = sc.nextLine();
            House house = verificarHouse(urb, model);
            if (house != null) {
                urb.getHouses().remove(house);
                System.out.println("La casa se ha eliminado con éxito");
            } else {
                System.out.println("La casa no existe!");
            }
        }
    }
    //DEPARTAMENTOS
    /**
     * Verifica si existe un modelo de departamento en la urbanización.
     * @param urb Urbanización que contiene el departamento
     * @param model Nombre del modelo de departamento
     * @return Dep si existe un departamento con el nombre ingresado por el usuario
     */
    public static Dep verificarDep(Urbanization urb, String model){
        for (Dep dep: urb.getDepartments()){
            if (dep.getModel()==model){
                return dep;
            }
        }
        return null;
    }
    
    /**
     * Añade un departamento a la urbanización ingresada.
     */
    public static void addDep() {
        System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if (verificarUrb(nameUrb, urbanizations) == null) {
            System.out.println("La urbanizacion no existe");
        } else {
            System.out.println("Ingrese nombre del nuevo modelo de departamento: ");
            String model = sc.nextLine();
            Dep dep = verificarDep(urb, model);
            
            if (dep != null) {
                System.out.println("Ese modelo ya existe");
            } else {
        
                System.out.println("Ingrese el número de Dormitorios: ");
                int noDorms = sc.nextInt();
                System.out.println("Ingrese el número de baños: ");
                int noBaths = sc.nextInt();
                System.out.println("Ingrese el tamaño del departamento en metros cuadrados: ");
                double size = sc.nextDouble();
                System.out.println("Ingrese el precio: ");
                float price = sc.nextFloat();
                boolean hasPark = true;

                String parkSN = "Error";
                while(parkSN.equals("Error")){
                    System.out.println("¿Posee parqueadero?: S/N");
                    parkSN = sc.nextLine();
                    if(parkSN.toUpperCase().equals("S")){
                        hasPark = true;
                    }
                    if(parkSN.toUpperCase().equals("N")){
                        hasPark = false;
                    }
                    else{
                        System.out.println("Opcion ingresada no válida.");
                        parkSN = "Error";
                    }
                }
                boolean hasLau = true;
                String lavSN = "Error";
                while(lavSN.equals("Error")){
                    System.out.println("¿Posee servicio de lavandería?: S/N");
                    lavSN = sc.nextLine();
                    if(lavSN.toUpperCase().equals("S")){
                        hasLau = true;
                    }
                    if(lavSN.toUpperCase().equals("N")){
                        hasLau = false;
                    }
                    else{
                        System.out.println("Opcion ingresada no válida.");
                        lavSN = "Error";
                    }
                }

                boolean hasElev = true;
                String elevSN = "Error";
                while(elevSN.equals("Error")){
                    System.out.println("¿Posee acceso a elevador?: S/N ");
                    elevSN = sc.nextLine();
                    if(elevSN.toUpperCase().equals("S")){
                        hasElev = true;
                    }
                    if(elevSN.toUpperCase().equals("N")){
                        hasElev = false;
                    }
                    else{
                        System.out.println("Opcion ingresada no válida.");
                        elevSN = "Error";
                    }
                }

                urb.getDepartments().add(new Dep(model,noDorms,noBaths,size, price, hasPark, hasLau, hasElev));

                System.out.println("El departamento se ha añadido con éxito");
            }

        }

    }

    /**
     * Muestra la información acerca de un departamento ingresado.
     */
    public static void showDep() {
        System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if (verificarUrb(nameUrb, urbanizations) == null) {
            System.out.println("La urbanizacion no existe");
        } else {
            System.out.println("Ingrese nombre del modelo del departamento que desea consultar: ");
            String model = sc.nextLine();
            Dep dep = verificarDep(urb, model);
            if (dep != null) {
                dep.toString();//Override this
            } else {
                System.out.println("El departamento no existe!");
            }

        }

    }

    /**
     * Borra un departamento de existente de la urbanización ingresada
     */
    public static void deleteDep() {
        System.out.println("Ingrese nombre de la urbanización: ");
        String nameUrb = sc.nextLine();
        Urbanization urb = verificarUrb(nameUrb, urbanizations);
        if (verificarUrb(nameUrb, urbanizations) == null) {
            System.out.println("La urbanizacion no existe");
        } else {
            System.out.println("Ingrese nombre del modelo de departamento que desea eliminar: ");
            String model = sc.nextLine();
            Dep dep = verificarDep(urb, model);
            if (dep != null) {
                urb.getDepartments().remove(dep);
                System.out.println("El departamento se ha eliminado con éxito");
            } else {
                System.out.println("El departamento no existe!");
            }
        }
    }

}
