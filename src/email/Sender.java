/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package email;
 
import email.Email;
import javax.swing.JOptionPane;

import java.util.Properties;
import javax.activation.*;
import javax.mail.*;
import javax.mail.internet.*;

import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;

/**
 *
 * @author Andres
 */
public class Sender extends javax.swing.JFrame{
    private Email email= new Email();

    public void SendMail(Email em)  {
        this.email= em;
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(email.getUsuarioCorreo(), email.getContrasena());
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("tucasayainmobiliaria9@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email.getDestino()));
            message.setSubject(email.getAsunto());
            
            BodyPart texto= new MimeBodyPart();
            texto.setText(email.getMensaje());
            BodyPart adjunto= new MimeBodyPart();
            if (!email.getRutaArchivo().equals("")){
                adjunto.setDataHandler(new DataHandler(new FileDataSource(email.getRutaArchivo())));
                adjunto.setFileName(email.getNombreArchivo());
                
            }
            MimeMultipart m=new MimeMultipart();
            m.addBodyPart(texto);
            m.addBodyPart(adjunto);
            
            
            message.setContent(m);
            

            Transport.send(message);
            JOptionPane.showMessageDialog(this, "Su mensaje ha sido enviado");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
