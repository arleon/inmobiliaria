/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package email;

/**
 *
 * @author Andres
 */
public class Email {
    private final String usuarioCorreo= "tucasayainmobiliaria9@gmail.com";

    private final String contrasena = "192837645" ;
    private String rutaArchivo;
    private String asunto;
    private String nombreArchivo;
    private String destino;
    private String mensaje;


    public Email(){}
    
    //sobrecarga de constructor

    public Email(String rutaArchivo, String asunto, String nombreArchivo, String destino, String mensaje) {
        
        this.rutaArchivo = rutaArchivo;
        this.asunto = asunto;
        this.nombreArchivo = nombreArchivo;
        this.destino = destino;
        this.mensaje = mensaje;
        
        
    }
   
    /**
     * @return the usuarioCorreo
     */
    public String getUsuarioCorreo() {
        return usuarioCorreo;
    }

    

    /**
     * @return the contrasena
     */
    public String getContrasena() {
        return contrasena;
    }

    
    /**
     * @return the rutaArchivo
     */
    public String getRutaArchivo() {
        return rutaArchivo;
    }

    /**
     * @param rutaArchivo the rutaArchivo to set
     */
    public void setRutaArchivo(String rutaArchivo) {
        this.rutaArchivo = rutaArchivo;
    }

    /** 
     * @return the asunto
     */
    public String getAsunto() {
        return asunto;
    }

    /**
     * @param asunto the asunto to set
     */
    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }
    
    

    /**
     * @return the nombreArchivo
     */
    public String getNombreArchivo() {
        return nombreArchivo;
    }

    /**
     * @param nombreArchivo the nombreArchivo to set
     */
    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    /**
     * @return the destino
     */
    public String getDestino() {
        return destino;
    }

    /**
     * @param destino the destino to set
     */
    public void setDestino(String destino) {
        this.destino = destino;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

 

}
