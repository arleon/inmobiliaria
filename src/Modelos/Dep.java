/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import inmobiliaria.Urbanization;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Angellina
 */
public class Dep extends Home {
    //Atributos
    private boolean hasPark;
    private boolean hasLau;
    private boolean hasElev;
    
    static Scanner sc = new Scanner(System.in);
    
    public boolean gethasPark(){
        return hasPark;
    }

    public boolean gethasLau(){
        return hasLau;
    }
    
    public boolean gethasElev(){
        return hasElev;
    }
    
    /**
     * Constructor de Departamento
     * @param model modelo del departamento
     * @param noDorms numero de dormitorios
     * @param noBaths numero de baños
     * @param size tamaño de departamento
     * @param price precio
     * @param hasPark si tiene parqueadero
     * @param hasLau si tiene servicio de lavandería
     * @param hasElev si tiene elevador
     */
    public Dep(String model, int noDorms, int noBaths, double size, double price, boolean hasPark, boolean hasLau, boolean hasElev){
        super(model,noDorms,noBaths,size,price);
        this.hasPark = hasPark;
        this.hasLau = hasLau;
        this.hasElev = hasElev;
    }
    
    /**
     * Añade un nuevo modelo de departamento a una urbanización existente
     */
    public void addDep(){
        System.out.println("Ingrese la urbanización");
        String u = sc.nextLine();
        Urbanization urb = null;
        //Urbanization urb = verificarUrb(u,getUrbanizations());
        //Agregar a Principalllll
        if(urb==null){
            System.out.println("Urbanización ingresada no existe");
        }
        else{
        System.out.println("Ingrese el modelo: ");
        String model = sc.nextLine();
        System.out.println("Ingrese el número de Dormitorios: ");
        int noDorms = sc.nextInt();
        System.out.println("Ingrese el número de baños: ");
        int noBaths = sc.nextInt();
        System.out.println("Ingrese el tamaño del departamento en metros cuadrados: ");
        double size = sc.nextDouble();
        System.out.println("Ingrese el precio: ");
        float price = sc.nextFloat();
        System.out.println("¿Posee parqueadero?: S/N");
        
        String parkSN = "";
        while(parkSN.equals("Error")){
            parkSN = sc.nextLine();
            if(parkSN.toUpperCase().equals("S")){
                boolean hasPark = true;
            }
            if(parkSN.toUpperCase().equals("N")){
                boolean hasPark = false;
            }
            else{
                System.out.println("Opcion ingresada no válida.");
                parkSN = "Error";
            }
        }
        
        System.out.println("¿Posee servicio de lavandería?: S/N");
        String lavSN = "";
        while(lavSN.equals("Error")){
            lavSN = sc.nextLine();
            if(lavSN.toUpperCase().equals("S")){
                boolean hasLau = true;
            }
            if(lavSN.toUpperCase().equals("N")){
                boolean hasLau = false;
            }
            else{
                System.out.println("Opcion ingresada no válida.");
                lavSN = "Error";
            }
        }
        
        System.out.println("¿Posee acceso a elevador?: S/N ");
        String elevSN = "";
        while(elevSN.equals("Error")){
            elevSN = sc.nextLine();
            if(elevSN.toUpperCase().equals("S")){
                boolean hasElev = true;
            }
            if(elevSN.toUpperCase().equals("N")){
                boolean hasElev = false;
            }
            else{
                System.out.println("Opcion ingresada no válida.");
                elevSN = "Error";
            }
        }
        
        urb.getDepartments().add(new Dep(model,noDorms,noBaths,size, price, hasPark, hasLau, hasElev));
        //Dep d = new Dep(model,noDorms,noBaths,size, price, hasPark, hasLau, hasElev);
        
        //Agregar a archh
        }
    }
    
    /**
     * Imprime la información del modelo del departamento ingresado.
     * @param model Modelo del departamento
     */
    public void showInfo(String model){
        System.out.println("Modelo: "+model);

        System.out.println("Dormitorios: " + getnoDorms() + "Baños: " + getnoBaths());

        if (hasPark || hasLau || hasElev){
            System.out.println("Adicionales disponibles:");
        }
        if(hasPark){
            System.out.println("- Parqueadero");            
        } 
        if(hasLau){
            System.out.println("- Lavandería");
        }
        if(hasElev){
            System.out.println("- Acceso a Elevador");
        }
        
    }
    
    @Override
    public String toString(){
        String a = "Modelo: "+getModel()+" **Dormitorios: "+ getnoDorms()+" **Baños: " + getnoBaths();
        if(gethasPark() == true){
            String pk = " - Parqueadero";
            a.concat(pk);
        }
        if(gethasLau() == true){
            String lau = "- Lavandería";
            a.concat(lau);
        }
        if(gethasElev() == true){
            String ele = " - Elevador";
            a.concat(ele);
        }
        return a;
                
    }
    
    /**
     * Genera un archivo con información del departamento ingresado por el usuario para luego enviarlo por correo
     * @param d Departamento que usuario desea conocer más información
     * @return ruta en que se encuentra el archivo generado
     * @throws IOException 
     */
    public String DepEmail(Dep d) throws IOException{
        String m = "Modelo: "+ d.getModel()+"\n Área de construcción: "+d.getSize()+"metros cuadrados\n";
        String e = "Ambientes: \n -Habitaciones: "+d.getnoDorms()+"\n -Baños: "+d.getnoBaths()+"\n";
        String ruta = "C:\\Users\\Andres\\Documents\\inmobiliaria\\archivoDep.txt";
        String mensaje =m+e;
        if(hasPark == true){
            String prk = "-Posee parqueadero\n";
            mensaje.concat(prk);
        }
        if(hasLau == true){
            String la = "-Posee lavandería";
            mensaje.concat(la);
        }
        if(hasElev == true){
            String el = "-Posee acceso a elevador";
            mensaje.concat(el);
        }
        String pr = "Precio: "+d.getPrice()+" dolares";
        mensaje.concat(pr);
        File archivo = new File(ruta);
        BufferedWriter bw;
        if(archivo.exists()){
            //existeee
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(mensaje);
        }
        else{
            //No existe la vaina
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(mensaje);
            
        }
        bw.close();
        
        return ruta;
    }

    @Override
    public int compareTo(Home o) {
        if (this.getPrice() < o.getPrice()) return -1;
        if (this.getPrice() > o.getPrice()) return 1;
        return 0;
    }
    
}
