/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import inmobiliaria.Urbanization;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Paula
 */
public class House extends Home{
    private int numFloors;
    private int numKit;
    private boolean servRoom;
        
    static Scanner sc = new Scanner(System.in);
    /**
     * Constructor de casa
     * @param model
     * @param noDorms
     * @param noBaths
     * @param size
     * @param price
     * @param numFloors número de plantas
     * @param numKit número de cocinas
     * @param servRoom si tiene cuarto de servicio
     */
    public House(String model, int noDorms, int noBaths, double size, double price, int numFloors, int numKit, boolean servRoom){
        super(model,noDorms,noBaths,size,price);
        this.numFloors = numFloors;
        this.numKit = numKit;
        this.servRoom = servRoom;
        
    }
    
    public int getnumFloors(){
        return numFloors;
    }
    
    public int getnumKit(){
        return numKit;
    }
    
    public boolean getservRoom(){
        return servRoom;
    }
    
    /**
     * Agrega un nuevo modelo de casa a una urbanización existente
     */
    public void addHouse(){
                System.out.println("Ingrese la urbanización");
        String u = sc.nextLine();
        Urbanization urb = null;
        //Agregar a a Principal Inmobiliaria ////
        //Urbanization urb = verificarUrb(u, getUrbanizations());
        if(urb==null){
            System.out.println("Urbanización ingresada no existe");
        }
        else{
            System.out.println("Ingrese el modelo: ");
            String model = sc.nextLine();
            System.out.println("Ingrese el número de dormitorios: ");
            int noDorms = sc.nextInt();
            System.out.println("Ingrese el número de baños: ");
            int noBaths = sc.nextInt();
            System.out.println("Ingrese el tamaño de la casa en metros cuadrados: ");
            double size = sc.nextDouble();
            System.out.println("Ingrese el precio: ");
            double precio = sc.nextDouble();
            System.out.println("Ingrese el número de plantas: ");
            int numFloor = sc.nextInt();
            System.out.println("Ingrese el número de cocinas: ");
            int numKit = sc.nextInt();
            System.out.println("¿Posee cuarto de servicio?: S/N");
            String roomSN = "";
            while(roomSN.equals("Error")){
                roomSN = sc.nextLine();
                if(roomSN.toUpperCase().equals("S")){
                    boolean servRoom = true;
                }
                if(roomSN.toUpperCase().equals("N")){
                    boolean servRoom = false;
                }
                else{
                    System.out.println("Opcion ingresada no válida.");
                    roomSN = "Error";
                }
            }
            
            urb.getHouses().add(new House(model,noDorms,noBaths,size,precio,numFloor,numKit,servRoom));
            //House h = new House(model,noDorms,noBaths,size,precio,numFloor,numKit,servRoom);
            //Agregar a archivo
        }
    }
    
    /**
     * Imprime la información del modelo de la casa ingresado.
     * @param model Modelo de la casa
     */
    public String showInfo(String model){
        if(servRoom){
            return "Modelo: "+model+" **Número de plantas: " + numFloors+" **Dormitorios: "
                    +getnoDorms()+" **Baños: "+getnoBaths() +" **Cocinas: "+numKit + " y Cuarto de Servicio";
        }
        else{
            return "Modelo: "+model+" **Número de plantas: " + numFloors+" **Dormitorios: "
                    +getnoDorms()+" **Baños: "+getnoBaths() +" **Cocinas: "+numKit;
        }
    }
    
    @Override
    public String toString(){
        if(servRoom){
            return "Modelo: "+getModel()+" **Número de plantas: " + getnumFloors()+
                    " **Dormitorios: "+getnoDorms()+" **Baños: "+getnoBaths() +" **Cocinas: "+numKit + 
                    " y Cuarto de Servicio\n ** Precio: "+getPrice()+" dolares.";
        }
        else{
            return "Modelo: "+getModel()+" **Número de plantas: " + getnumFloors()+
                    " **Dormitorios: "+getnoDorms()+" **Baños: "+getnoBaths() +" **Cocinas: "+numKit +
                    "\n ** Precio: "+getPrice()+" dolares.";
        }
    }
    
    /**
     * Genera un documento con la información de la casa para enviarlo por correo al cliente
     * @param casa Casa que usuario desees conocer más información
     * @return ruta en donde se encuentre el documento
     * @throws IOException 
     */
    public String CasaEmail(House casa) throws IOException{
       
        String m = "Modelo: "+ casa.getModel()+"\nÁrea de construcción: "+casa.getSize()+"metros cuadrados\n";
        String p = "Casa de "+casa.getnumFloors()+" plantas";
        String e = "Ambientes:\n -"+casa.getnoDorms()+"habitaciones\n-"+casa.getnoBaths()+"baños\n-"+casa.getnumKit()+" cocinas\n";
        String mensaje =m+p+e;
        if(servRoom == true){
            String sr = "-Posee cuarto de servicios\n";
            mensaje.concat(sr);
        }
        String pr = "Precio: "+casa.getPrice()+" dolares";
        mensaje.concat(pr);
        String ruta = "C:\\Users\\Andres\\Documents\\inmobiliaria\\archivoHouse.txt";
        File archivo = new File(ruta);
        BufferedWriter bw;
        if(archivo.exists()){
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(mensaje);
        }
        else{
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(mensaje);
        }
        bw.close();

        return ruta;
    }
    

    @Override
    public int compareTo(Home o) {
        if (this.getPrice() < o.getPrice()) return -1;
        if (this.getPrice() > o.getPrice()) return 1;
        return 0;
    }
    }



    
   

