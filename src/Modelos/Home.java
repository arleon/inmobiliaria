/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;

/**
 *
 * @author Paula
 */
public abstract class Home implements Comparable<Home>, Serializable{

    //Atributos
    private String model;
    private int noDorms;
    private int noBaths;
    private double size;
    private double price;
    
    /**
     * 
     * @param model modelo casa
     * @param noDorms número dormitorios
     * @param noBaths número baños
     * @param size tamaño
     * @param price precio
     */
    public Home(String model, int noDorms, int noBaths, double size, double price){
        this.model = model;
        this.noDorms = noDorms;
        this.noBaths = noBaths;
        this.size = size;
        this.price = price;
        
    }
    
    public void setModel (String model){
        this.model = model;
    }
    
    public String getModel (){
        return model;
    }
    
    public void setnoDorms(int noDorms){
        this.noDorms = noDorms;
    }
    
    public int getnoDorms(){
        return noDorms;
    }
    
    public void setnoBaths(int noBaths){
        this.noBaths = noBaths;
    }
    
    public int getnoBaths(){
        return noBaths;
    }
    
    public void setSize(double size){
        this.size = size;
    }
    
    public double getSize(){
        return size;
    }
    
    public void setPrice(double price){
        this.price = price;
    }
    
    public double getPrice(){
        return price;
    }

    @Override
    public abstract int compareTo(Home h);
    

}
